import cv2
import numpy as np
import math
import copy

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
image_original = cv2.imread(picPath + 'timg.jpg')
image = copy.copy(image_original)
image_temp = None

drawing = False
mode = True  # rectangle/circle
gx, gy = -1, -1


# mouse callback function
def draw_(event, x, y, flags, param):
    global image, image_temp, drawing, mode, gx, gy
    if event == cv2.EVENT_LBUTTONDOWN:
        gx, gy = x, y
        drawing = True
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        image_temp = None
        if mode == True:
            cv2.rectangle(image, (gx, gy), (x, y), (255, 0, 0), 2)
        else:
            radius = int(round(math.sqrt(math.pow(gx-x, 2) + math.pow(gy-y, 2)), 0))
            cv2.circle(image, (gx, gy), radius, (0, 255, 0), 2)
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            image_temp = copy.copy(image)
            if mode == True:
                cv2.rectangle(image_temp, (gx, gy), (x, y), (255, 0, 0), 2)
            else:
                radius = int(round(math.sqrt(math.pow(gx - x, 2) + math.pow(gy - y, 2)), 0))
                cv2.circle(image_temp, (gx, gy), radius, (0, 255, 0), 2)
            cv2.imshow('image', image_temp)

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.setMouseCallback('image', draw_)

while 1:
    if drawing == False:
        cv2.imshow('image', image)
    key = cv2.waitKey(60) & 0xff
    if key == 27:  # ESC
        break
    elif key == 9:  # Tab
        mode = not mode
    elif key == ord('r'):  # reset
        image = copy.copy(image_original)
        cv2.imshow('image', image)

cv2.destroyAllWindows()

