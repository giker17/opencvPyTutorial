import numpy as np
import cv2
from matplotlib import pyplot as plt

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
image = cv2.imread(picPath + 'timg.jpg')

b, g, r = cv2.split(image)
image2 = cv2.merge([r, g, b])

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.namedWindow('image2', cv2.WINDOW_NORMAL)
cv2.imshow('image', image)
cv2.imshow('image2', image2)

plt.subplot(121);plt.imshow(image) # expects distorted color
plt.subplot(122);plt.imshow(image2) # expect true color
plt.show()

cv2.destroyAllWindows()

