import numpy as np
import cv2
from matplotlib import pyplot as plt

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
image = cv2.imread(picPath + 'timg.jpg')

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.imshow('image', image)

plt.imshow(image, cmap = 'gray', interpolation = 'bicubic')
plt.xticks([]), plt.yticks([])  # to hide tick values on X and Y axis
plt.show()

cv2.waitKey(0)
cv2.destroyAllWindows()

