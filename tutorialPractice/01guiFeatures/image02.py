import numpy as np
import cv2

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
image = cv2.imread(picPath + 'timg1.jpg')

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.imshow('image', image)

k = cv2.waitKey(0)
if k == ord('s'):
    cv2.imwrite('image01.png', image)

cv2.destroyAllWindows()

