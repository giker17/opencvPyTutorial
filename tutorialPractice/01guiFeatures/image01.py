import numpy as np
import cv2

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'

image1 = cv2.imread(picPath + '66491779_11.jpg', 1)
image0 = cv2.imread(picPath + '66491779_11.jpg', 0)
image_1 = cv2.imread(picPath + '66491779_11.jpg', -1)

cv2.imshow('image1', image1)  #cv2.IMREAD_COLOR
cv2.imshow('image0', image0)  #cv2.IMREAD_GRAYSCALE
cv2.imshow('image_1', image_1) #cv2.IMREAD_UNCHANGED
cv2.waitKey(0)
cv2.destroyAllWindows()
