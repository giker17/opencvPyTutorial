import cv2
import numpy as np
from time import sleep

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img1 = cv2.imread(picPath + '66491779_22.jpg')
img2 = cv2.imread(picPath + '66491779_13.jpg')
img1 = img1[0:637]

cv2.namedWindow('img', cv2.WINDOW_NORMAL)
cv2.imshow('img', img1)
sleep(0.5)

w1 = 1
while 1:
    key = cv2.waitKey(50) & 0xff
    if key == 27:
        break
    w1 = w1 - 0.01
    if w1 >= 0:
        img = cv2.addWeighted(img1, w1, img2, 1-w1, 0)
        cv2.imshow('img', img)

cv2.destroyAllWindows()

