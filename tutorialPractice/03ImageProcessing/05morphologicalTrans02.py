import cv2
import numpy as np
import matplotlib.pyplot as plt

''' morphological transformation
    形态学变换
'''

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img = cv2.imread(picPath + '66491779_11.jpg')

kernel = np.ones((3,3), np.uint8)
gradient = cv2.morphologyEx(img, cv2.MORPH_GRADIENT, kernel)
tophat = cv2.morphologyEx(img, cv2.MORPH_TOPHAT, kernel)
blackhat = cv2.morphologyEx(img, cv2.MORPH_BLACKHAT, kernel)

cv2.imshow('img', img)
cv2.imshow('gradient', gradient)
cv2.imshow('tophat', tophat)
cv2.imshow('blackhat', blackhat)
cv2.waitKey(0)

cv2.destroyAllWindows()

