import cv2
import numpy as np
import matplotlib.pyplot as plt

''' image thresholding -- Otsu's Binarization
    图像阈值过滤
    http://blog.csdn.net/lbing9002/article/details/70276271
'''

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img = cv2.imread(picPath + 'prspcTrans2_2.jpg')
row, col, channel = img.shape

img2 = cv2.resize(img, (int(col/4), int(row/4)),interpolation = cv2.INTER_CUBIC)
gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
# cv2.imshow('gray2', gray2)

# simple image thresholding
success, profile0 = cv2.threshold(gray2, 100, 255, cv2.THRESH_BINARY)

# Otsu's thresholding
success, profile1 = cv2.threshold(gray2, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

# Otsu's thresholding after Gaussian filtering
gau = cv2.GaussianBlur(gray2, (3,3), 0)
success, profile2 = cv2.threshold(gau, 0, 255, cv2.THRESH_BINARY+cv2.THRESH_OTSU)

#'''
#plt.subplot(2, 2, 1), plt.imshow(gray2, 'gray'), plt.title('gray'), plt.xticks([]), plt.yticks([])
plt.subplot(3, 3, 1), plt.imshow(gray2, 'gray'), plt.title('original'), plt.xticks([]), plt.yticks([])
plt.subplot(3, 3, 2), plt.hist(gray2.ravel(), 256), plt.title('Histogram'), plt.xticks([]), plt.yticks([])
plt.subplot(3, 3, 3), plt.imshow(profile0, 'gray'), plt.title('simpleThresholding'), plt.xticks([]), plt.yticks([])
plt.subplot(3, 3, 4), plt.imshow(gray2, 'gray'), plt.title('original'), plt.xticks([]), plt.yticks([])
plt.subplot(3, 3, 5), plt.hist(gray2.ravel(), 256), plt.title('Histogram'), plt.xticks([]), plt.yticks([])
plt.subplot(3, 3, 6), plt.imshow(profile1, 'gray'), plt.title('otsuThresholding'), plt.xticks([]), plt.yticks([])

plt.subplot(3, 3, 7), plt.imshow(gau, 'gray'), plt.title('original-gausian'), plt.xticks([]), plt.yticks([])
plt.subplot(3, 3, 8), plt.hist(gau.ravel(), 256), plt.title('Histogram-gausian'), plt.xticks([]), plt.yticks([])
plt.subplot(3, 3, 9), plt.imshow(profile1, 'gray'), plt.title('otsuThresholding-gausian'), plt.xticks([]), plt.yticks([])
plt.show()
'''
cv2.imshow('profile1', profile1)
cv2.imshow('profile2', profile2)
cv2.waitKey(0)
#'''

cv2.destroyAllWindows()

