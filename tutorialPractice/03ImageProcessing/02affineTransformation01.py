import cv2
import numpy as np

''' scaling '''

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img = cv2.imread(picPath + '66491779_11.jpg')

# '''
res = cv2.resize(img,None,fx=2, fy=2, interpolation = cv2.INTER_CUBIC)
'''
# OR
height, width = img.shape[:2]
res = cv2.resize(img,(2*width, 2*height), interpolation = cv2.INTER_CUBIC)
# '''
# cv2.namedWindow('res', cv2.WINDOW_NORMAL)

cv2.imshow('img', img)
cv2.imshow('res', res)
cv2.waitKey(0)
cv2.destroyAllWindows()



