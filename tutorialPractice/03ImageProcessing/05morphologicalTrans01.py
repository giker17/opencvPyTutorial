import cv2
import numpy as np
import matplotlib.pyplot as plt

''' morphological transformation
    形态学变换
    http://blog.csdn.net/app_12062011/article/details/27351043
'''

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img = cv2.imread(picPath + '66491779_11.jpg')

kernel = np.ones((3,3), np.uint8)
erosion = cv2.erode(img, kernel, iterations = 1)
dilation = cv2.dilate(img, kernel, iterations = 1)
opening = cv2.morphologyEx(img, cv2.MORPH_OPEN, kernel)
closing = cv2.morphologyEx(img, cv2.MORPH_CLOSE, kernel)

cv2.imshow('img', img)
cv2.imshow('erosion', erosion)
cv2.imshow('dilation', dilation)
cv2.imshow('opening', opening)
cv2.imshow('closing', closing)
cv2.waitKey(0)

cv2.destroyAllWindows()

