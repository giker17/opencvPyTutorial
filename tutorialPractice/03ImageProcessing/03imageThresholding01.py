import cv2
import numpy as np
import matplotlib.pyplot as plt

''' image thresholding
    图像阈值过滤
    http://blog.csdn.net/lbing9002/article/details/70276271
'''

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img = cv2.imread(picPath + 'prspcTrans2_2.jpg')
row, col, channel = img.shape

img2 = cv2.resize(img, (int(col/4), int(row/4)),interpolation = cv2.INTER_CUBIC)
gray2 = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
# cv2.imshow('gray2', gray2)

# simple image thresholding
success, profile0 = cv2.threshold(gray2, 100, 255, cv2.THRESH_TOZERO)

# adaptive image thresholding -- mean
profile1 = cv2.adaptiveThreshold(gray2, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY, 11, 10)

# adaptive image thresholding -- gaussian
profile2 = cv2.adaptiveThreshold(gray2, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 11, 10)

'''
plt.subplot(2, 2, 1), plt.imshow(gray2, 'gray'), plt.title('gray')
plt.subplot(2, 2, 2), plt.imshow(profile0, 'gray'), plt.title('simpleThresholding')
plt.subplot(2, 2, 3), plt.imshow(profile1, 'gray'), plt.title('adaptiveThresholding-mean')
plt.subplot(2, 2, 4), plt.imshow(profile1, 'gray'), plt.title('adaptiveThresholding-gausian')
plt.show()
'''
cv2.imshow('profile1', profile1)
cv2.imshow('profile2', profile2)
cv2.waitKey(0)
#'''

cv2.destroyAllWindows()

