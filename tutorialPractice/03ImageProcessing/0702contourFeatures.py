import cv2

''' contours - features
'''

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img = cv2.imread(picPath + 'hand.jpg')
row, col, channel = img.shape
img2 = cv2.resize(img, (int(col / 8), int(row / 8)), interpolation=cv2.INTER_CUBIC)
gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

cv2.imshow('img2 original', img2)

# simple image thresholding
success, simple = cv2.threshold(gray, 150, 255, cv2.THRESH_BINARY_INV)
simple2, contours, hierarchy = cv2.findContours(simple, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(img2, contours, -1, (0, 255, 0), 2)

canny = cv2.Canny(gray, 50, 170)

# corner = cv2.cornerHarris(gray, 2, 23, 0.04)
# cv2.imshow('corner', corner)
cv2.imshow('simple', simple)
cv2.imshow('canny', canny)
cv2.imshow('img2', img2)

cv2.waitKey(0)
cv2.destroyAllWindows()

