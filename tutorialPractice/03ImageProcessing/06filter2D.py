import cv2
import numpy as np

''' filter2D
    通用过滤器
    http://blog.csdn.net/liyuanbhu/article/details/48718735
'''

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img = cv2.imread(picPath + '66491779_11.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
img = gray

# kernel = np.float32([[0,-1,0],[-1,5,-1],[0,-1,0]])
kernelSobel = np.float32([[-1,-2,-1],[0,0,0],[1,2,1]])
kernelHor = np.float32([-2,0,2])
kernelVer = np.float32([-2,0,2])
# kernel1 = np.float32([[0,-1,0],[0,1,0],[0,0,0]])
kernelSobel1 = np.float32([[-1,-1.414,-1],[0,0,0],[1,1.414,1]])
kernelScharr = np.float32([[-3,-10,-3],[0,0,0],[3,10,3]])

imgHor = cv2.filter2D(img, -1, kernelSobel)
imgVer = cv2.filter2D(img, -1, kernelVer)
imgHorVer = cv2.sepFilter2D(img, -1, kernelHor, kernelVer)
imgSobel = cv2.filter2D(img, -1, kernelSobel1)
imgScharr = cv2.filter2D(img, -1, kernelScharr)

cv2.imshow('filtered image horizon', imgHor)
cv2.imshow('filtered image vertical', imgVer)
cv2.imshow('filtered image hor-ver', imgHorVer)
cv2.imshow('filtered image sobel', imgSobel)
cv2.imshow('filtered image Scharr', imgScharr)
cv2.waitKey(0)

cv2.destroyAllWindows()

