import cv2

''' contours - getting started
    For better accuracy, use binary images.
    So before finding contours, apply threshold or canny edge detection.
'''

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img = cv2.imread(picPath + 'hand.jpg')
row, col, channel = img.shape
img2 = cv2.resize(img, (int(col/8), int(row/8)),interpolation = cv2.INTER_CUBIC)
gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

# simple image thresholding
success, simple = cv2.threshold(gray, 150, 255, cv2.THRESH_BINARY_INV)
simple2, contours, hierarchy = cv2.findContours(simple, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(img2, contours, -1, (0,255,0), 2)

cv2.imshow('simple', simple)
cv2.imshow('img2', img2)

cv2.waitKey(0)
cv2.destroyAllWindows()

