import cv2
import numpy as np

videoPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/video/'
cap = cv2.VideoCapture(videoPath + 'mayun01.f4v')

while(1):
    # Take each frame
    _, frame = cap.read()
    # Convert BGR to HSV
    hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)
    # define range of blue color in HSV
    lower_blue = np.array([0,0,200])
    upper_blue = np.array([250,255,255])
    # Threshold the HSV image to get only blue colors
    mask = cv2.inRange(hsv, lower_blue, upper_blue)
    # Bitwise-AND mask and original image
    res = cv2.bitwise_and(frame,frame, mask= mask)
    cv2.imshow('frame',frame)
    cv2.imshow('mask',mask)
    cv2.imshow('res',res)
    k = cv2.waitKey(35) & 0xFF
    if k == 27:
        break
cv2.destroyAllWindows()



