import cv2

''' contours - getting started
    For better accuracy, use binary images.
    So before finding contours, apply threshold or canny edge detection.
'''

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img = cv2.imread(picPath + 'horse.jpg')
gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

# simple image thresholding
# success, simple = cv2.threshold(gray, 127, 255, cv2.THRESH_BINARY_INV)
success, simple = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY_INV)

# adaptive image thresholding -- mean
mean = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV, 11, 10)

# adaptive image thresholding -- gaussian
gaussian = cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY_INV, 11, 10)

# canny edge
canny = cv2.Canny(gray, 100, 200)

simple2, contours, hierarchy = cv2.findContours(simple, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(img, contours, -1, (0,255,0), 2)
'''
mean2, contoursMean, hierarchyMean = cv2.findContours(mean, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(img, contoursMean, -1, (0,255,0), 3)
canny2, contoursCanny, hierarchyCanny = cv2.findContours(canny, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(img, contoursCanny, -1, (0,255,0), 3)

gau2, contoursGau, hierarchyGau = cv2.findContours(gaussian, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
cv2.drawContours(img, contoursGau, -1, (0,255,0), 3)
# '''

# cv2.imshow('gray', gray)
cv2.imshow('simple', simple)
cv2.imshow('mean', mean)
cv2.imshow('canny', canny)
cv2.imshow('gaussian', gaussian)
cv2.imshow('result', img)

cv2.waitKey(0)

cv2.destroyAllWindows()

