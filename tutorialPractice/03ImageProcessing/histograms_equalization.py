import os

# equalization using numpy
def np_equalization(img):
    hist, bins = np.histogram(img.flatten(), 256, [0,256])
    cdf = hist.cumsum()
    cdf_normalization = cdf * hist.max()/cdf.max()

    plt.plot(cdf_normalization, color='b')
    plt.hist(img.flatten(), 256, [0,256], color='r')
    plt.xlim([0,256])
    plt.legend(('cdf', 'histogram'), loc='upper left')

    plt.show()

# equalization using numpy
def np_equalization1(img):
    hist, bins = np.histogram(img.flatten(), 256, [0, 256])
    cdf = hist.cumsum()

    cdf_m = np.ma.masked_equal(cdf, 0)
    cdf_m = (cdf_m - cdf_m.min()) * 256 / (cdf_m.max() - cdf_m.min())
    cdf = np.ma.filled(cdf_m, 0).astype('uint8')

    img2 = cdf[img]
    cv2.imshow('img2', img2)
    cv2.waitKey(0)

# equalization in opencv
def cv_normalization(imggray):
    equ = cv2.equalizeHist(imggray)
    res = np.hstack((imggray, equ))
    cv2.namedWindow('res', cv2.WINDOW_NORMAL)
    cv2.imshow('res', res)
    cv2.waitKey(0)

# clahe
def clahe(img):
    clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))

    cl0 = clahe.apply(img[:,:,0])
    cl1 = clahe.apply(img[:,:,1])
    cl2 = clahe.apply(img[:,:,2])
    cl = cv2.merge((cl0, cl1, cl2))
    cv2.namedWindow('cl', cv2.WINDOW_NORMAL)
    cv2.imshow('cl', cl)
    cv2.waitKey(0)


datapath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures'
imgpath = os.path.join(datapath, '66491779_13.jpg')  # 13, 19

if __name__ == '__main__':
    import numpy as np
    import cv2
    from matplotlib import pyplot as plt

    img = cv2.imread(imgpath, 1)
    imggray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imshow('img', img)

    # np
    # np_equalization(img)
    # np_equalization1(img)

    # opencv
    # cv_normalization(imggray)

    # clahe
    clahe(img)

    cv2.destroyAllWindows()

