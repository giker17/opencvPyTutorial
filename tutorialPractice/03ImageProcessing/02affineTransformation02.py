import cv2
import numpy as np
import math

''' affine transformation
    仿射变换：平移(translation)，缩放(scale)，旋转(rotation)，错切(Shear)，反转(flip)（5种）
    http://blog.csdn.net/carson2005/article/details/7540936
    错切：x,y轴变为非直角
'''

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img = cv2.imread(picPath + '66491779_11.jpg')
row, col, channel = img.shape

'''
# shear-y
theta = 30
radians = theta * math.pi / 180
M = np.float32([[math.cos(radians),0,0],[-math.sin(radians),1,int(col/4)]])
img = cv2.warpAffine(img, M, (col, row))

# shear-x
theta = 30
radians = theta * math.pi / 180
M = np.float32([[1,-math.sin(radians),int(col/4)],[0,math.cos(radians),0]])
img = cv2.warpAffine(img, M, (col, row))

# rotation
theta = 30
radians = theta * math.pi / 180
M = np.float32([[math.cos(radians),-math.sin(radians),int(col/2)],[math.sin(radians),math.cos(radians),0]])
img = cv2.warpAffine(img, M, (col, row))

# flip
M = np.float32([[1,0,0],[0,-1,row]])
img = cv2.warpAffine(img, M, (col, row))

# translation
M = np.float32([[1,0,20],[0,1,30]])
img = cv2.warpAffine(img, M, (col, row))

# scale-2
img = cv2.resize(img, (2*col, 2*row),interpolation = cv2.INTER_CUBIC)

# scale-0.5
img = cv2.resize(img, (int(col/2), int(row/2)),interpolation = cv2.INTER_CUBIC)
# '''

cv2.imshow('img', img)
cv2.waitKey(0)
cv2.destroyAllWindows()

