import os
import numpy as np
import cv2
from matplotlib import pyplot as plt

# histogram calculation in opencv
def cv_hist(img):
    hist = cv2.calcHist([img], [0], None, [256], [0,256])
    plt.plot(hist)
    plt.show()

# histogram calculation in numpy
def np_hist(imggray):
    hist, bins = np.histogram(imggray.ravel(), 256, [0,256])
    plt.plot(hist)
    plt.show()

# plot histograms use Matplotlib plotting functions
def plt_hist(imggray):
    plt.hist(imggray.ravel(), 256, [0, 256])
    plt.show()

# calculate histogram for each color channel using Matplotlib
def plt_hist_channel(img):
    color = ('b', 'g', 'r')
    for i, c in enumerate(color):
        histr = cv2.calcHist([img], [i], None, [256], [0, 256])
        plt.plot(histr, color=c)
        plt.xlim([0,256])
    plt.show()


datapath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures'
imgpath = os.path.join(datapath, '66491779_13.jpg')  # 19

if __name__ == '__main__':
    img = cv2.imread(imgpath, 1)
    imggray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    cv2.imshow('img', img)
    # cv_hist(imggray)
    # np_hist(imggray)
    # plt_hist(imggray)
    plt_hist_channel(img)

