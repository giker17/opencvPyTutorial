import cv2
import numpy as np
import math
import copy

''' perspective transformation
    透视变换
    http://blog.csdn.net/xiaowei_cqu/article/details/26471527
'''


def callBack(event, x, y, flags, params):
    global points, img
    if event == cv2.EVENT_LBUTTONDOWN:
        points.extend([[x,y]])
        img = cv2.circle(img, (x, y), 15, (0,255,0), -1)
        cv2.imshow('img', img)
        if len(points) == 4:
            size = 800
            # points0 = np.float32([[1078,1607],[2363,1399],[1094,2855],[3244,2402]])
            points0 = np.float32(points)
            points1 = np.float32([[0, 0], [size, 0], [0, size], [size, size]])
            M = cv2.getPerspectiveTransform(points0, points1)

            rslt = cv2.warpPerspective(img, M, (size, size))
            cv2.imshow('rslt', rslt)

picPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/pictures/'
img_original = cv2.imread(picPath + 'prspcTrans2.jpg')
img = copy.copy(img_original)
cv2.namedWindow('img', cv2.WINDOW_NORMAL)
row, col, channel = img.shape
points = []
cv2.setMouseCallback('img', callBack)
cv2.imshow('img', img)

while 1:
    key = cv2.waitKey(35) & 0xff
    if key == 27:
        break
    elif key == ord('r'):
        points = []
        img = copy.copy(img_original)
        cv2.imshow('img', img)

cv2.destroyAllWindows()

