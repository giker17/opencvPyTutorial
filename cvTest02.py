import cv2 as cv
import numpy as np

img = cv.imread('D:/Python/workspace/Pictures/0001.jpg')
rows, cols, ch = img.shape

pts1 = np.float32([[50, 50], [200, 50], [50, 200]])
pts2 = np.float32([[10, 100], [200, 50], [100, 250]])

M = cv.getAffineTransform(pts1, pts2)

dst = cv.warpAffine(img, M, (cols, rows))

cv.imshow('image01', dst)
cv.waitKey(0)