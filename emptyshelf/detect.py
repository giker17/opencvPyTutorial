import numpy as np
import cv2

dataPath = 'D:/Python/workspace/PycharmSpace/opencvPython/emptyshelf/'

emptyshelf_cascade = cv2.CascadeClassifier(dataPath + 'cascade_60_30.xml')

a, b = 1.3, 5
img1 = cv2.imread(dataPath + 'shelf1.jpg')
img2 = cv2.imread(dataPath + 'shelf2.jpg')
img3 = cv2.imread(dataPath + 'shelf3.jpg')
img4 = cv2.imread(dataPath + 'shelf4.jpg')
shelves1 = emptyshelf_cascade.detectMultiScale(img1, a, b)
shelves2 = emptyshelf_cascade.detectMultiScale(img2, a, b)
shelves3 = emptyshelf_cascade.detectMultiScale(img3, a, b)
shelves4 = emptyshelf_cascade.detectMultiScale(img4, a, b)

for (x,y,w,h) in shelves1:
    cv2.rectangle(img1,(x,y),(x+w,y+h),(255,0,0),2)
for (x,y,w,h) in shelves2:
    cv2.rectangle(img2,(x,y),(x+w,y+h),(255,0,0),2)
for (x,y,w,h) in shelves3:
    cv2.rectangle(img3,(x,y),(x+w,y+h),(255,0,0),2)
for (x,y,w,h) in shelves4:
    cv2.rectangle(img4,(x,y),(x+w,y+h),(255,0,0),2)

cv2.namedWindow('img', cv2.WINDOW_NORMAL)
cv2.imshow('img', img4)
# cv2.imshow('img2', img2)
cv2.waitKey(0)
cv2.destroyAllWindows()