import os
import numpy as np
import cv2

homepath = 'D:/Python/workspace/PycharmSpace/opencvPython/'
videopath = os.path.join(homepath, 'data/video/emptyshelf/')
outputpath = os.path.join(videopath, 'output')
if not os.path.exists(outputpath):
    os.mkdir(outputpath)

emptyshelf_cascade = cv2.CascadeClassifier(os.path.join(homepath, 'emptyshelf/test_short_supply_sjj.xml'))
emptyshelf_cascade = cv2.CascadeClassifier(os.path.join(homepath, 'emptyshelf/cascade_24_24_19.xml'))
emptyshelf_cascade = cv2.CascadeClassifier(os.path.join(homepath, 'emptyshelf/cascade_48_48_6.xml'))
emptyshelf_cascade = cv2.CascadeClassifier(os.path.join(homepath, 'emptyshelf/cascade_48_48_7.xml'))
emptyshelf_cascade = cv2.CascadeClassifier(os.path.join(homepath, 'emptyshelf/cascade_shortsupply_24_24_19.xml'))
video1 = os.path.join(videopath, 'IMG_0159.MOV')
video2 = os.path.join(videopath, 'IMG_0160.MOV')

cap = cv2.VideoCapture(video2)
cv2.namedWindow('img', cv2.WINDOW_NORMAL)

scaleFactor, minNeighbors = 1.1, 3
isoutput = False
i = 0
while 1:
    success, frame = cap.read()
    if not success:
        break
    if i%1 == 0:
        shelves = emptyshelf_cascade.detectMultiScale(frame, scaleFactor, minNeighbors,
                                                      cv2.CASCADE_SCALE_IMAGE, (150, 150))
        for (x, y, w, h) in shelves:
            cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
        if isoutput and shelves is not None and len(shelves) != 0:
            cv2.imwrite(os.path.join(outputpath, str(i) + '.jpg'), frame)
        cv2.imshow('img', frame)
    i += 1
    key = cv2.waitKey(1) & 0xff
    if key == 27:  # ESC
        break

cv2.destroyAllWindows()
