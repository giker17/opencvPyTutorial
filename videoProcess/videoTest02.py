import numpy as np
import cv2

dataPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/'

faceCascade = cv2.CascadeClassifier(dataPath + 'xml/haarcascade_frontalface_default.xml')
eyeCascade = cv2.CascadeClassifier(dataPath + 'xml/haarcascade_eye.xml')

cap = cv2.VideoCapture(dataPath + 'video/20170417T151504Z.mp4')
cap = cv2.VideoCapture(dataPath + 'video/mayun01.f4v')

frame = cap.read()
while 1:
    success, frame = cap.read()
    if not success:
        break
    grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    faces = faceCascade.detectMultiScale(grayFrame, 1.3, 5, cv2.CASCADE_SCALE_IMAGE, (45, 50))

    for (x, y, w, h) in faces:
        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
        grayFace = grayFrame[y : y + h, x : x + w]
        face = frame[y : y + h, x : x + w]
        eyes = eyeCascade.detectMultiScale(grayFace, 1.1, 0, cv2.CASCADE_SCALE_IMAGE, (45, 50))
        for (ex, ey, ew, eh) in eyes:
            cv2.rectangle(face, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)

    cv2.imshow('video', frame)

    k = cv2.waitKey(60) & 0xff
    if k == 27:
        break
    else:
        cv2.imwrite('video_' + str(k) + '.jpg', frame)

cv2.waitKey(0)
cv2.destroyAllWindows()
cap.release()
