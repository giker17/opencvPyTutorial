import os,sys
import cv2
import numpy as np

datapath = 'D:/softwares/opencv/train_test/emptyshelf/posdata_process/process3/'
shotcut = os.path.join(datapath, 'shotcut')

files = [filename for filename in os.listdir(shotcut)
         if os.path.isfile(os.path.join(shotcut, filename))
            and (filename.endswith('.JPG')
                 or filename.endswith('.jpg')
                 or filename.endswith('.PNG')
                 or filename.endswith('.png')
                 or filename.endswith('.JPEG')
                 or filename.endswith('.jpeg'))]

rsltpath = os.path.join(datapath, 'emptyshelf_pos')
if not os.path.exists(rsltpath) or not os.path.isdir(rsltpath):
    os.mkdir(rsltpath)

infofile = os.path.join(rsltpath, 'info.txt')

i = 0
with open(infofile, 'w') as info:
    for f in files:
        tmpname = str(i) + '.jpg'
        tmppath = os.path.join(rsltpath, tmpname)
        '''
        if os.path.exists(tmppath):
            os.remove(tmppath)
        os.rename(os.path.join(datapath,f), tmppath)
        # '''

        img = cv2.imread(os.path.join(shotcut, f))
        h, w, channel = img.shape

        cv2.imwrite(tmppath, img)

        info.write('posdata/' + tmpname + ' 1 0 0 ' + str(w) + ' ' + str(h) + '\n')
        i += 1



cv2.destroyAllWindows()

r'''
create vec file of posdata:
D:\softwares\opencv\opencv3.2\opencv\build\x64\vc14\bin\opencv_createsamples -vec pos.vec -info info.txt -num 317 [-w 48 -h 48]

train:
D:\softwares\opencv\opencv3.2\opencv\build\x64\vc14\bin\opencv_traincascade.exe -data result_48 -vec pos.vec -bg neg.txt -numPos 300 -numNeg 1000 -numStages 20 -w 48 -h 48 -minHitRate 0.997 -featureType HAAR -mode ALL
'''

