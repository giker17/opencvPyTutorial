import cv2 as cv

#faceDetector = cv.CascadeClassifier('D:/Python/workspace/Setting/lbpcascade_frontalface.xml')
faceDetector = cv.CascadeClassifier('D:/Python/workspace/Setting/haarcascade_frontalface_default.xml')
eye_cascade = cv.CascadeClassifier('D:/Python/workspace/Setting/haarcascade_eye.xml')
image = cv.imread('D:/Python/workspace/Pictures/0002.jpg', 1)

gray = cv.cvtColor(image, cv.COLOR_BGR2GRAY)
faces = faceDetector.detectMultiScale(image)
print(faces)
for (x,y,w,h) in faces:
    image = cv.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)
    '''
    roi_gray = gray[y:y + h, x:x + w]
    roi_color = image[y:y + h, x:x + w]
    eyes = eye_cascade.detectMultiScale(roi_gray)
    for (ex, ey, ew, eh) in eyes:
        cv.rectangle(roi_color, (ex, ey), (ex + ew, ey + eh), (0, 255, 0), 2)
    #'''

cv.imshow('image', image)
cv.waitKey(0)


