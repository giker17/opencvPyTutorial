import numpy as np
import cv2


# mouse callback function
def draw_circle(event, x, y, flags, param):
    if event == cv2.EVENT_LBUTTONDBLCLK:
        cv2.circle(image, (x, y), 10, (255, 0, 0), -1)
    elif event == cv2.EVENT_MOUSEWHEEL:
        print(event)
        print(x, y)
        # cv2.resizeWindow('image', 10, 20)

picPath = 'D:/download/Pictures/'
image = cv2.imread(picPath + 'IMG_20170503_144553_BURST53.jpg')
# image = np.zeros((512, 512, 3), np.uint8)

cv2.namedWindow('image', cv2.WINDOW_NORMAL)
cv2.setMouseCallback('image', draw_circle)

while 1:
    cv2.imshow('image', image)
    if cv2.waitKey(60) & 0xff == 27:
        break

cv2.destroyAllWindows()


