import numpy as np
import cv2
import os



def rectFace(picPath, fileName, rsltPath, x, y ,w, h):
    filePath = picPath + fileName
    img = cv2.imread(filePath)

    # cv2.rectangle(img, (x, y), (x + w, y + h), (255, 0, 0), 2)
    image = img[y:y+h,x:x+w,:]
    cv2.imwrite(rsltPath + fileName, image, [int(cv2.IMWRITE_JPEG_QUALITY), 100])

def main():
    picPath = 'D:/download/Pictures/pic2/'
    paths = os.listdir(picPath)
    for p in paths:
        if os.path.isfile(picPath + p):
            rectFace(picPath=picPath, fileName=p, rsltPath=picPath+'result/'
                     , x=1807, y=1240, w=630, h=750)
    cv2.destroyAllWindows()

main()




