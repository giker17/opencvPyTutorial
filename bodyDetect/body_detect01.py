import numpy as np
import cv2

dataPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/'

full_body_cascade = cv2.CascadeClassifier(dataPath + 'xml/haarcascade_fullbody.xml')
lower_body_cascade = cv2.CascadeClassifier(dataPath + 'xml/haarcascade_lowerbody.xml')
upper_body_cascade = cv2.CascadeClassifier(dataPath + 'xml/haarcascade_upperbody.xml')

cap = cv2.VideoCapture(dataPath + 'video/body.mp4')
# cap = cv2.VideoCapture(dataPath + 'video/V70511-171432.mp4')
# cap = cv2.VideoCapture(dataPath + 'video/VID_20170511_171045.mp4')
# cap = cv2.VideoCapture(dataPath + 'video/campus/4.qsv.FLV')

scaleFactor = 1.2
minNeightbors = 2
frame = cap.read()
while 1:
    success, frame = cap.read()
    if not success:
        break
    grayFrame = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    full_bodies = lower_bodies = upper_bodies =[]
    full_bodies = full_body_cascade.detectMultiScale(grayFrame, scaleFactor, minNeightbors,
                                                     cv2.CASCADE_SCALE_IMAGE, (45, 50))
    # lower_bodies = lower_body_cascade.detectMultiScale(grayFrame, scaleFactor, minNeightbors,
    #                                                    cv2.CASCADE_SCALE_IMAGE, (45, 50))
    # upper_bodies = upper_body_cascade.detectMultiScale(grayFrame, scaleFactor, minNeightbors,
    #                                                    cv2.CASCADE_SCALE_IMAGE, (45, 50))

    for (x, y, w, h) in full_bodies:
        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (255, 0, 0), 2)
    for (x, y, w, h) in lower_bodies:
        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 255, 0), 2)
    for (x, y, w, h) in upper_bodies:
        frame = cv2.rectangle(frame, (x, y), (x + w, y + h), (0, 0, 255), 2)

    cv2.imshow('video', frame)

    k = cv2.waitKey(1) & 0xff
    if k == 27:
        break

# cv2.waitKey(0)
cv2.destroyAllWindows()
cap.release()



