import numpy as np
import cv2

dataPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/'

face_cascade = cv2.CascadeClassifier(dataPath + 'xml/haarcascade_frontalface_default.xml')

img = cv2.imread(dataPath + 'image/Pictures/faces.jpg')
faces = face_cascade.detectMultiScale(img, 1.3, 5)

for (x,y,w,h) in faces:
    cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)

cv2.imshow('img', img)
cv2.waitKey(0)
cv2.destroyAllWindows()


