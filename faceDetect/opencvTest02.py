import numpy as np
import cv2

dataPath = 'D:/Python/workspace/PycharmSpace/opencvPython/data/'

classifier = cv2.CascadeClassifier(dataPath + 'image/resultData_20170414/cascade.xml')
#classifier = cv2.CascadeClassifier(dataPath + 'image/testdog_haar.xml')

image = cv2.imread(dataPath + 'image/Pictures/dog2.jpg')


gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

dogFaces = classifier.detectMultiScale(gray, 1.02, 3, cv2.CASCADE_SCALE_IMAGE,(45,50))
print(dogFaces)

for (x,y,w,h) in dogFaces:
    cv2.rectangle(image,(x,y),(x+w,y+h),(255,0,0),2)

cv2.imshow('image', image)
cv2.waitKey(0)


